# Django Enviroments

Entornos **Docker** para desarrollo en **Django**.

## Instalación

1. Preparamos el archivo webapp/**Dockerfile** con la imagen de Python.

2. Preparamos el archivo webapp/**requirements.txt**.

3. Preparamos el archivo **docker-dompose.yml**.

4. Creamos un proyecto de **Django** utilizando el contenedor.

docker-compose run web django-admin.py startproject django_environments webapp 

5. Accedemos a **settings.py** para configurar la base de datos de **PostgreSQL**.

``` DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'midb',
        'USER': 'usuariodb',
        'PASSWORD': 'secreto'
        'HOST': 'db' # el nombre del contenedor de la base de datos
        'PORT': '5432',
    }
}
```
Al volver a levantar los contenedores una vez creadro el proyeto hay que montar el volumen.

docker volume create --name=db_data

Volvemos a levantar los contenedores. ;a primera vez que ejecutemos el comando si da error porque, aunque se creó la base de datos, todavía no está disponible, volver a ejecutar el comando.

docker-compose up-d